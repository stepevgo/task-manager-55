package ru.t1.stepanishchev.tm.endpoint;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.service.IServiceLocator;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.dto.request.AbstractUserRequest;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.exception.user.AccessDeniedException;

public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    private final IServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        return getServiceLocator().getAuthService().validateToken(token);
    }

    @NotNull
    protected SessionDTO check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (request == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        return getServiceLocator().getAuthService().validateToken(token);
    }

}