package ru.t1.stepanishchev.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.dto.model.SessionDTO;
import ru.t1.stepanishchev.tm.dto.model.TaskDTO;
import ru.t1.stepanishchev.tm.dto.model.UserDTO;
import ru.t1.stepanishchev.tm.model.Project;
import ru.t1.stepanishchev.tm.model.Session;
import ru.t1.stepanishchev.tm.model.Task;
import ru.t1.stepanishchev.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

import static org.hibernate.cfg.AvailableSettings.*;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = getEntityManagerFactory();
    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    @NotNull
    public EntityManagerFactory getEntityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDatabaseDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getDatabaseURL());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getDatabaseUsername());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getDatabasePassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, propertyService.getDatabaseDialect());
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, propertyService.getDatabaseHbm2DdlAuto());
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, propertyService.getDatabaseShowSql());

        settings.put(USE_SECOND_LEVEL_CACHE, propertyService.getDBSecondLvlCash());
        settings.put(CACHE_REGION_FACTORY, propertyService.getDBFactoryClass());
        settings.put(USE_QUERY_CACHE, propertyService.getDBUseQueryCash());
        settings.put(USE_MINIMAL_PUTS, propertyService.getDBUseMinPuts());
        settings.put(CACHE_REGION_PREFIX, propertyService.getDBRegionPrefix());
        settings.put(CACHE_PROVIDER_CONFIG, propertyService.getDBHazelConfig());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources source = new MetadataSources(registry);
        source.addAnnotatedClass(UserDTO.class);
        source.addAnnotatedClass(User.class);
        source.addAnnotatedClass(ProjectDTO.class);
        source.addAnnotatedClass(Project.class);
        source.addAnnotatedClass(TaskDTO.class);
        source.addAnnotatedClass(Task.class);
        source.addAnnotatedClass(SessionDTO.class);
        source.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = source.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Override
    public void close() {
        entityManagerFactory.close();
    }

}