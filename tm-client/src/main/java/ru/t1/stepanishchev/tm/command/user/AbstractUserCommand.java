package ru.t1.stepanishchev.tm.command.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.stepanishchev.tm.api.endpoint.IAuthEndpoint;
import ru.t1.stepanishchev.tm.api.endpoint.IUserEndpoint;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

@Component
public abstract class AbstractUserCommand extends AbstractCommand {

    @Autowired
    protected IUserEndpoint userEndpoint;

    @Autowired
    protected IAuthEndpoint authEndpoint;

}