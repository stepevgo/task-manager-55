package ru.t1.stepanishchev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class ApplicationVersionCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "version";

    @NotNull
    private final String ARGUMENT = "-v";

    @NotNull
    private final String DESCRIPTION = "Show app version.";

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getApplicationVersion());
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}