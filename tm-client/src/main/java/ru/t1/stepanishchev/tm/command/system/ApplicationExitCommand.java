package ru.t1.stepanishchev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ApplicationExitCommand extends AbstractSystemCommand {

    @NotNull
    private final String NAME = "exit";

    @NotNull
    private final String DESCRIPTION = "Close application";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

}